/**
 * Module Dependencies
 */

const axios = require('axios')
const axiosRetry = require('axios-retry')

axiosRetry(axios, { retries: 3 })

/**
 * Export `driver`
 */

module.exports = driver

/**
 * Default HTTP driver
 *
 * @param {Object} opts
 * @return {Function}
 */

function driver(opts) {
  opts = opts || {
    headers: {
      'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
    }
  }

  return function http_driver(ctx, fn) {
    axios(ctx.url, opts)
      .then(res => {
        ctx.status = res.status
        Object.assign(ctx.headers, res.headers)
        ctx.body = res.data
        return fn(null, ctx)
      })
      .catch(err => {
        return fn(err)
      })
  }
}
